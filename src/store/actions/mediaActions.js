export const createMedia = (media) => {
  return (dispatch, getState, { getFirestore }) => {
    const firestore = getFirestore();
    firestore.collection('media').doc(media.mediaId).set({
      ...media
    }).then(() => {
      dispatch({ type: 'CREATE_MEDIA_SUCCESS' });
    }).catch(err => {
      dispatch({ type: 'CREATE_MEDIA_ERROR' }, err);
    });
  }
};

export const deleteMedia = (media) => {
  return (dispatch, getState, { getFirestore }) => {
    const firestore = getFirestore();
    firestore.collection('media').doc(media.mediaId).delete().then(() => {
      dispatch({ type: 'DELETE_MEDIA_SUCCESS' });
    }).catch(err => {
      dispatch({ type: 'DELETE_MEDIA_ERROR' }, err);
    });
  }
};