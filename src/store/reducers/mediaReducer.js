const initState = {}

const mediaReducer = (state = initState, action) => {
  switch (action.type) {
    case 'CREATE_MEDIA_SUCCESS':
      console.log('create media success');
      return state;
    case 'CREATE_MEDIA_ERROR':
      console.log('create media error');
      return state;
    case 'DELETE_MEDIA_SUCCESS':
      console.log('delete media success');
      return state;
    case 'DELETE_MEDIA_ERROR':
      console.log('delete media error');
      return state;
    default:
      return state;
  }
};

export default mediaReducer;