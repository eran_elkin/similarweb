import React, { Component } from 'react'
import Toolbar from '../components/toolbar';
import MediaList from '../components/mediaList';

export class MediaPanel extends Component {
  render() {
    return (
      <div className="media-left-panel">
        <Toolbar />
        <MediaList />
      </div>
    )
  }
}

export default MediaPanel
