import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

var config = {
  apiKey: "AIzaSyDywbNW3Jos4jFDy_85oMbNVGoo0BRbY_E",
  authDomain: "similarweb-1ba82.firebaseapp.com",
  databaseURL: "https://similarweb-1ba82.firebaseio.com",
  projectId: "similarweb-1ba82",
  storageBucket: "similarweb-1ba82.appspot.com",
  messagingSenderId: "667064476372",
  appId: "1:667064476372:web:d65e1b4871518b3e5b5141",
  measurementId: "G-T06PTZ2TSH"
};
firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true });


export default firebase 