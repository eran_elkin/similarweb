import React, { Component } from 'react';
import MediaPanel from './containers/mediaPanel';
import MediaPlayer from './components/mediaPlayer';

class App extends Component {
  render() {
    return (
      <div className="similarweb-app">
        <img src="/img/sw.png" className="similarweb-img" />
        <div className="similarweb-content">
          <MediaPanel />
          <MediaPlayer />
        </div>
      </div>
    );
  }
}

export default App;
