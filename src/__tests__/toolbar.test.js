import { Toolbar, mapDispatchToProps } from '../components/toolbar';

it('should mount the component correctly', () => {
  const wrapper = shallow(<Toolbar />);
  expect(wrapper).toMatchSnapshot();
})

it('test mapDispatchToProps', () => {
  expect(mapDispatchToProps(() => { }).createMedia).toBeTruthy;
});

it('test keyup input field, addMedia function should not invoke when keycode NOT 13', () => {
  const onMediaInputKeyup = jest.fn();
  const wrapper = shallow(<Toolbar />);

  wrapper.find('input').simulate('keyup', { keyCode: 10 });
  expect(onMediaInputKeyup.mock.calls.length).toBe(0);
});

it('test addMedia, should invoke createMedia', () => {
  const url = 'abc?de<THIS IS MY KEY>&ghi'
  const createMedia = jest.fn();
  const wrapper = shallow(<Toolbar createMedia={createMedia} />);

  wrapper.instance().addMedia(url);
  expect(createMedia.mock.calls[0][0].mediaId).toBe('<THIS IS MY KEY>');
});