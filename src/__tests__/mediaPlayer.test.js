import { MediaPlayer, mapDispatchToProps } from '../components/mediaPlayer';

it('should mount the component correctly', () => {
  const wrapper = shallow(<MediaPlayer />);
  expect(wrapper).toMatchSnapshot();
})

it('test mapDispatchToProps', () => {
  expect(mapDispatchToProps(() => { }).deleteMedia).toBeTruthy;
});

it('should not render youtube player when mediaList empty', () => {
  const mediaList = [];
  const wrapper = mount(
    <MediaPlayer mediaList={mediaList} />
  )
  expect(wrapper.find('YouTube').length).toBe(0);
})

it('should render always one youtube player when mediaList with media', () => {
  const mediaList = [{}, {}];
  const wrapper = mount(
    <MediaPlayer mediaList={mediaList} />
  )
  expect(wrapper.find('YouTube').length).toBe(1);
})

it('test addMedia, should invoke createMedia', () => {
  const deleteMedia = jest.fn();
  const wrapper = shallow(<MediaPlayer mediaList={[{ mediaId: '123' }]}
    deleteMedia={deleteMedia} />);

  wrapper.instance().onVideoEnd();
  expect(deleteMedia.mock.calls[0][0]).toEqual({ mediaId: '123' });
});