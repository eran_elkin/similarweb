import { Media, mapDispatchToProps } from '../components/media';
import { wrap } from 'module';

it('should mount the component correctly', () => {
  const wrapper = shallow(<Media media={{ mediaId: 1 }} />);
  expect(wrapper).toMatchSnapshot();
})

it('test media name, should be like the props', () => {
  const media = {
    createdAt: 1574532229966,
    mediaId: 'MY NAME'
  }
  const wrapper = shallow(<Media media={media} />);
  expect(wrapper.find('.media-name').text()).toBe('MY NAME');
})

it('test media time, should be like the props(20:03)', () => {
  const timestamp = 1574532229966;
  const media = {
    createdAt: timestamp,
    mediaId: 'MY NAME'
  }
  const wrapper = shallow(<Media media={media} />);
  expect(wrapper.find('.media-time').text()).toBe('20:03');
})

it('test onKeyUp input field, onMediaInputKeyup function should be called', () => {
  const media = {
    createdAt: 1574532229966,
    mediaId: 'MY NAME'
  }
  const deleteMedia = jest.fn();
  const wrapper = shallow(<Media media={media} deleteMedia={deleteMedia} />);
  wrapper.find('button').simulate('click');
  expect(deleteMedia.mock.calls[0][0]).toEqual({ mediaId: 'MY NAME' });
});

it('test mapDispatchToProps', () => {
  expect(mapDispatchToProps(() => { }).deleteMedia).toBeTruthy;
});