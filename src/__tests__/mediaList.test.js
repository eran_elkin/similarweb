import { MediaList, mapStateToProps } from '../components/mediaList';

it('should mount the component correctly', () => {
  const wrapper = shallow(<MediaList />);
  expect(wrapper).toMatchSnapshot();
})

it('test the Media amount in the list, should be empty', () => {
  const media = [];
  const wrapper = mount(
    <MediaList mediaList={media} />
  )
  expect(wrapper.find('Media').length).toBe(0);
})

it('test the Media amount in the list, should be 2', () => {
  const media = [{ mediaId: '123' }, { mediaId: '133' }];
  const wrapper = shallow(
    <MediaList mediaList={media} />
  )
  expect(wrapper.find('Connect(Media)').length).toBe(2);
})

it('test mapStateToProps, return right value', () => {
  const state = {
    firestore: {
      ordered: {
        media: 'ONE MEDIA'
      }
    }
  }
  const retVal = mapStateToProps(state);
  expect(retVal).toEqual({ mediaList: 'ONE MEDIA' });
})