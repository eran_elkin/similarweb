import React, { Component } from 'react'
import { connect } from 'react-redux';
import { deleteMedia } from '../store/actions/mediaActions';

class Media extends Component {
  onDeleteClick = () => {
    const { media } = this.props;

    this.props.deleteMedia({ mediaId: media.mediaId });
  }
  render() {
    const { media, addClass } = this.props;
    const date = new Date(media.createdAt);
    const hour = (date.getHours() < 10 ? '0' : '') + date.getHours().toLocaleString();
    const min = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes().toLocaleString();

    return (
      <div className={`media-item ${addClass}`}>
        <div className="media-name">{media.mediaId}</div>
        <div className="media-time">{`${hour}:${min}`}</div>
        <button onClick={this.onDeleteClick}>X</button>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    deleteMedia: (media) => dispatch(deleteMedia(media))
  }
}

export default connect(null, mapDispatchToProps)(Media);
export { Media, mapDispatchToProps };