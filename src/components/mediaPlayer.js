import React from 'react';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import YouTube from 'react-youtube';
import { deleteMedia } from '../store/actions/mediaActions';

class MediaPlayer extends React.Component {
  onVideoEnd = () => {
    const { mediaList } = this.props;
    const mediaId = mediaList[mediaList.length - 1].mediaId;

    this.props.deleteMedia({ mediaId });
  }

  render() {
    const { mediaList = [] } = this.props;
    const lastMedia = mediaList[mediaList.length - 1]
    const opts = {
      height: '400',
      width: '640',
      playerVars: {
        autoplay: 1
      }
    };

    return (
      <div className="media-player">
        {lastMedia && <YouTube
          videoId={lastMedia.mediaId}
          opts={opts}
          onReady={this.onVideoReady}
          onEnd={this.onVideoEnd}
        />}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    mediaList: state.firestore.ordered.media
  }
}

const mapDispatchToProps = dispatch => {
  return {
    deleteMedia: (media) => dispatch(deleteMedia(media))
  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firestoreConnect([
    { collection: 'media', orderBy: ['createdAt'] },
    { collection: 'notifications' }
  ])
)(MediaPlayer)

export { MediaPlayer, mapDispatchToProps };