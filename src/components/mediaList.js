import React, { Component } from 'react';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import Media from './media';

class MediaList extends Component {
  render() {
    const { mediaList } = this.props;

    return (
      <div className="media-list">
        {mediaList && mediaList.map((media, index) => {
          return (
            <Media key={index} media={media} addClass={mediaList.length - 1 === index ? 'playing-song' : ''} />
          )
        })
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    mediaList: state.firestore.ordered.media
  }
}

export default compose(
  connect(mapStateToProps),
  firestoreConnect([
    { collection: 'media', orderBy: ['createdAt'] },
  ])
)(MediaList);

export { MediaList, mapStateToProps };