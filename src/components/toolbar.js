import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createMedia } from '../store/actions/mediaActions';

class Toolbar extends Component {
  constructor(props) {
    super(props);

    this.txtUrl = React.createRef();
    this.state = {
      mediaId: '',
      createdAt: 0
    }
  }
  onMediaInputKeyup = (ev) => {
    if (ev.keyCode === 13) {
      console.log(this.txtUrl)
      this.addMedia(this.txtUrl.current.value);
      this.txtUrl.current.value = '';
    }
  }
  onAddMediaClick = (ev) => {
    const url = this.txtUrl.current.value;

    if (url !== '') {
      ev.preventDefault();
      this.addMedia(url);
      this.txtUrl.current.value = '';
    }
  }
  addMedia = (url) => {
    const { createMedia } = this.props;

    createMedia({
      mediaId: url.split('?')[1].split('&')[0].substr(2),
      createdAt: new Date().getTime()
    });
  }

  render() {
    return (
      <div className="toolbar">
        <input id="url" type="text" className="toolbar-text"
          ref={this.txtUrl}
          onKeyUp={this.onMediaInputKeyup}
          placeholder="Enter a media Url..." />
        <button className="toolbar-btn" onClick={this.onAddMediaClick}>Add</button>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    createMedia: (media) => dispatch(createMedia(media))
  }
}

export default connect(null, mapDispatchToProps)(Toolbar);
export { Toolbar, mapDispatchToProps };